#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from datetime import datetime
import gzip
import io
import logging
from collections import namedtuple
import os
import re
from string import Template
import tempfile
import time


CONFIG = {
    "REPORT_SIZE": 100,
    "REPORTS_DIR": "./reports",
    "TIMESTAMP_FILE": "./log_analyzer.ts",
    "LOG_DIR": "./log",
    "REPORT_TEMPLATE_PATH": "./template/report_template.html",
    "ERRORS_LIMIT": "20"
}

# log_format ui_short '$remote_addr $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

LOG_RECORD_RE = re.compile(
    '^'
    '\S+ '  # remote_addr
    '\S+\s+'  # remote_user (note: ends with double space)
    '\S+ '  # http_x_real_ip
    '\[\S+ \S+\] '  # time_local [datetime tz] i.e. [29/Jun/2017:10:46:03 +0300]
    '"\S+ (?P<href>\S+) \S+" '  # request "method href proto" i.e. "GET /api/v2/banner/23815685 HTTP/1.1"
    '\d+ '  # status
    '\d+ '  # body_bytes_sent
    '"\S+" '  # http_referer
    '".*" '  # http_user_agent
    '"\S+" '  # http_x_forwarded_for
    '"\S+" '  # http_X_REQUEST_ID
    '"\S+" '  # http_X_RB_USER
    '(?P<time>\d+\.\d+)'  # request_time
)

LogFileInfo = namedtuple('LogFileInfo', ['file_path', 'file_date'])


def get_latest_log_file_info(logs_dir):
    if not os.path.isdir(logs_dir):
        return None

    latest_file_info = None

    for filename in os.listdir(logs_dir):
        match = re.match(r'^nginx-access-ui\.log-(?P<date>\d{8})(\.gz)?$', filename)
        if not match:
            continue

        date_string = match.group('date')
        file_date = datetime.strptime(date_string, "%Y%m%d")

        if not latest_file_info or file_date > latest_file_info.file_date:
            latest_file_info = LogFileInfo(file_path=os.path.join(logs_dir, filename),
                                           file_date=file_date)

    return latest_file_info


def update_timestamp(file_path, timestamp):
    timestamp = int(timestamp)
    ts_dir = os.path.dirname(file_path)
    if not os.path.isdir(ts_dir):
        os.makedirs(ts_dir)

    with open(file_path, 'w') as ts_file:
        ts_file.write(str(timestamp))

    atime = os.stat(file_path).st_atime
    os.utime(file_path, (atime, timestamp))


def parse_log_record(log_line):
    match = LOG_RECORD_RE.match(log_line)
    if not match:
        logging.error('Error parsing line: "{}"'.format(log_line.rstrip()))
        return None

    href = match.group('href')
    request_time = float(match.group('time'))

    return href, request_time


def get_log_records(log_path, errors_limit=None):
    open_fn = gzip.open if log_path.endswith(".gz") else io.open
    errors = records = 0

    with open_fn(log_path, mode='rb') as log_file:
        for line in log_file:
            records += 1
            line = line.decode('utf8')
            record = parse_log_record(line)
            if not record:
                errors += 1
                continue

            yield record

    if errors_limit is not None and records > 0 and ((int(errors_limit) / float(100) - errors / float(records)) < 0):
        raise Exception('error limit exceeded')


def create_report(records, max_records):
    total_records = 0
    total_time = 0
    intermediate_data = {}

    for href, response_time in records:
        total_records += 1
        total_time += response_time
        create_or_update_intermediate_item(intermediate_data, href, response_time)

    sorted_values = sorted(intermediate_data.itervalues(), key=lambda i: i['response_time_avg'], reverse=True)
    sorted_values = sorted_values[:max_records]

    return [create_report_item(intermediate_item, total_records, total_time) for intermediate_item in sorted_values]


def create_or_update_intermediate_item(intermediate_data, href, response_time):
    item = intermediate_data.get(href)
    if not item:
        item = {'href': href,
                'requests_count': 0.,
                'response_time_sum': 0.,
                'max_response_time': response_time,
                'response_time_avg': 0.,
                'all_responses_time': []}
        intermediate_data[href] = item

    item['requests_count'] += 1
    item['response_time_sum'] += response_time
    item['max_response_time'] = max(item['max_response_time'], response_time)
    item['response_time_avg'] = item['response_time_sum'] / item['requests_count']
    item['all_responses_time'].append(response_time)


def create_report_item(intermediate_item, total_records, total_time):
    url = intermediate_item['href']
    count = intermediate_item['requests_count']
    count_perc = intermediate_item['requests_count'] / float(total_records) * 100
    time_avg = intermediate_item['response_time_avg']
    time_max = intermediate_item['max_response_time']
    time_med = median(intermediate_item['all_responses_time'])
    time_perc = intermediate_item['response_time_sum'] / total_time * 100
    time_sum = intermediate_item['response_time_sum']

    return {
        'url': url,
        'count': count,
        'count_perc': round(count_perc, 3),
        'time_avg': round(time_avg, 3),
        'time_max': round(time_max, 3),
        'time_med': round(time_med, 3),
        'time_perc': round(time_perc, 3),
        'time_sum': round(time_sum, 3)
    }


def median(values_list):
    if not values_list:
        return None
    l = sorted(values_list)
    size = len(l)
    if size % 2 == 1:
        return l[size / 2]
    else:
        return 0.5 * (l[size / 2 - 1] + l[size / 2])


def render_template(template_path, to, data):

    target_dir = os.path.dirname(to)
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)

    with open(template_path, 'rb') as template_file:
        template_string = template_file.read().decode('utf8')
        template = Template(template_string)

    rendered = template.safe_substitute(table_json=json.dumps(data))

    dirname, basename = os.path.split(to)

    with tempfile.NamedTemporaryFile(prefix=basename, dir=dirname) as temp:
        rendered = rendered.encode('utf8')
        temp.write(rendered)
        os.link(temp.name, to)


def main(conf_dict):
    latest_log_file_info = get_latest_log_file_info(conf_dict['LOG_DIR'])
    if not latest_log_file_info:
        logging.info('No correct log files in {}'.format(conf_dict['LOG_DIR']))
        return

    report_date_string = latest_log_file_info.file_date.strftime("%Y.%m.%d")
    report_filename = "report-{}.html".format(report_date_string)
    report_file_path = os.path.join(conf_dict['REPORTS_DIR'], report_filename)

    if os.path.isfile(report_file_path):
        logging.info("Already update")
        return

    log_records = get_log_records(latest_log_file_info.file_path, config.get('ERRORS_LIMIT'))
    report_data = create_report(log_records, config['REPORT_SIZE'])

    if not report_data:
        logging.exception('there is no data on this format')
        import sys
        sys.exit(1)

    render_template(config['REPORT_TEMPLATE_PATH'], report_file_path, report_data)

    logging.info('Report saved to {}'.format(os.path.normpath(report_file_path)))

    update_timestamp(config['TIMESTAMP_FILE'], time.time())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c',
        '--config',
        type=argparse.FileType(mode='r'),
        help='Path to log analyzer config file')
    args = parser.parse_args()

    config = CONFIG.copy()

    if args.config:
        try:
            import json
            user_config = json.load(args.config)
        except Exception:
            print 'Invalid config file'
            raise
        config.update(user_config)

    from utils import initial_logger
    try:
        initial_logger(config.get('MONITORING_LOG_FILE'))
    except Exception:
        print 'Error with initial logger'
        raise

    try:
        main(config)
    except Exception as e:
        logging.exception(e)
