# Log Analyzer
## Config:

Default config:
```python
CONFIG = {
    "REPORT_SIZE": 100,
    "REPORTS_DIR": "./reports",
    "TIMESTAMP_FILE": "./log_analyzer.ts",
    "LOG_DIR": "./log",
    "REPORT_TEMPLATE_PATH": "./template/report_template.html",
    "ERRORS_LIMIT": "20"
}
```
Consist from next fields:
* *REPORT_SIZE* - Max report size. <br>
* *REPORTS_DIR* - Reports path to directory. <br>
* *TIMESTAMP_FILE* - Path to timestamp file. <br>
* *LOGS_DIR* - Logs path directory.<br>
* *MONITORING_LOG_FILE* - Program log file path.<br>
* *REPORT_TEMPLATE_PATH* - Path to template to render reports.<br>
* *ERRORS_LIMIT* - Maximum percentage of records that can be unpaired. <br>

## Usage:
```
python log_analyzer.py [--config CONFIG_PATH]

optional arguments:
  -c, --config      config file path (json format)
```

## Tests:
```
python -m unittest  discover -s tests -v
```
