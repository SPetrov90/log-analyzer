# -*- coding: utf-8 -*-

import logging
import os


def initial_logger(path=None):
    if path and not os.path.isdir(path):
        os.makedirs(path)
    logging.basicConfig(
        filename=path,
        level=logging.INFO,
        format='[%(asctime)s] %(levelname).1s %(message)s',
        datefmt='%Y.%m.%d %H:%M:%S'
    )
