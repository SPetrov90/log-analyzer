{
    "REPORT_SIZE": 150,
    "REPORTS_DIR": "./reports",
    "TIMESTAMP_FILE": "./log_analyzer.ts",
    "LOG_DIR": "./log",
    "REPORT_TEMPLATE_PATH": "./template/report_template.html",
    "ERRORS_LIMIT": "20"
}