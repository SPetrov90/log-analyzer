import logging
import unittest
import os
import log_analyzer
from datetime import datetime

logging.root.disabled = True


class TestLogAnalyzer(unittest.TestCase):

    def test_get_latest_log_file_info_plain(self):
        plain_log_dir = os.path.join(os.path.dirname(__file__), 'test_data')

        file_path, file_date = log_analyzer.get_latest_log_file_info(plain_log_dir)

        self.assertEquals(file_date, datetime(2018, 5, 13, 0, 0))
        self.assertEquals(file_path,
                          os.path.join(os.path.dirname(__file__), 'test_data', 'nginx-access-ui.log-20180513'))

    def test_get_latest_log_file_info_gzip(self):
        gzip_log_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'gzip')

        file_path, file_date = log_analyzer.get_latest_log_file_info(gzip_log_dir)

        self.assertEquals(file_date, datetime(2018, 5, 13, 0, 0))
        self.assertEquals(file_path,
                          os.path.join(os.path.dirname(__file__), 'test_data', 'gzip',
                                       'nginx-access-ui.log-20180513.gz'))

    def test_parse_log_record(self):
        line = (
            '1.196.116.32 '
            '- '
            ' - '
            '[29/Jun/2017:03:50:22 +0300] '
            '"GET /api/v2/banner/25019354 HTTP/1.1" '
            '200 '
            '927 '
            '"-" '
            '"Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" '
            '"-" '
            '"1498697422-2190034393-4708-9752759" '
            '"dc7161be3" '
            '0.390'
        )

        href, response_time = log_analyzer.parse_log_record(line)
        self.assertEqual(href, '/api/v2/banner/25019354')
        self.assertAlmostEqual(response_time, 0.390)

    def test_parse_log_record_not_match(self):
        line = 'Bad line'

        record = log_analyzer.parse_log_record(line)

        self.assertIsNone(record)

    def test_parse_log_file_plain(self):
        plain_log_file = os.path.join(os.path.dirname(__file__), 'test_data', 'nginx-access-ui.log-20180513')
        records = list(log_analyzer.get_log_records(plain_log_file))
        self.assertEqual(len(records), 3)

    def test_parse_log_file_gzip(self):
        gzip_log_file = os.path.join(os.path.dirname(__file__), 'test_data', 'gzip', 'nginx-access-ui.log-20180513.gz')
        records = list(log_analyzer.get_log_records(gzip_log_file))
        #  the third line is incorrect
        self.assertEqual(len(records), 2)

    def test_create_or_update_intermediate_item_create(self):
        intermediate_data = {}
        href = '/api/some_url'
        response_time = 0.777

        log_analyzer.create_or_update_intermediate_item(intermediate_data, href, response_time)

        self.assertEqual(len(intermediate_data), 1)
        self.assertIn(href, intermediate_data)

        item = intermediate_data[href]

        self.assertEqual(item['href'], href)
        self.assertEqual(item['requests_count'], 1)
        self.assertAlmostEqual(item['response_time_sum'], response_time)
        self.assertAlmostEqual(item['max_response_time'], response_time)
        self.assertAlmostEqual(item['response_time_avg'], response_time)
        self.assertListEqual(item['all_responses_time'], [response_time])

    def test_create_or_update_intermediate_item_update(self):
        intermediate_data = {
            '/api/some_url': {
                'href': '/api/some_url',
                'requests_count': 1,
                'max_response_time': 0.11,
                'response_time_avg': 0.11,
                'response_time_sum': 0.11,
                'all_responses_time': [0.11]
            }
        }

        href = '/api/some_url'
        response_time = 0.33

        log_analyzer.create_or_update_intermediate_item(intermediate_data, href, response_time)

        self.assertEqual(len(intermediate_data), 1)
        self.assertIn(href, intermediate_data)

        item = intermediate_data[href]

        self.assertEqual(item['href'], href)
        self.assertEqual(item['requests_count'], 2)
        self.assertAlmostEqual(item['response_time_sum'], 0.44)
        self.assertAlmostEqual(item['max_response_time'], 0.33)
        self.assertAlmostEqual(item['response_time_avg'], 0.22)
        self.assertListEqual(item['all_responses_time'], [0.11, 0.33])

    def test_create_report_item(self):
        total_time = 5.0
        total_records = 15

        href = '/api/some_url'
        requests_count = 5
        responses = [0.11, 0.15, 0.13, 0.12, 0.10]
        response_time_sum = 0.61
        max_response_time = 0.15
        response_time_avg = 0.122

        intermediate_item = {'href': href,
                             'requests_count': requests_count,
                             'response_time_sum': response_time_sum,
                             'max_response_time': max_response_time,
                             'response_time_avg': response_time_avg,
                             'all_responses_time': responses}

        result_item = log_analyzer.create_report_item(intermediate_item, total_records, total_time)

        self.assertEqual(result_item['url'], href)
        self.assertEqual(result_item['count'], 5)
        self.assertAlmostEqual(result_item['count_perc'], 33.333, delta=0.001)
        self.assertAlmostEqual(result_item['time_avg'], 0.122, delta=0.001)
        self.assertAlmostEqual(result_item['time_max'], 0.15, delta=0.001)
        self.assertAlmostEqual(result_item['time_med'], 0.12, delta=0.001)
        self.assertAlmostEqual(result_item['time_perc'], 12.2, delta=0.001)
        self.assertAlmostEqual(result_item['time_sum'], 0.61, delta=0.001)

    def test_median_even(self):
        data = [1, 2, 3, 4, 5, 6]
        self.assertAlmostEqual(log_analyzer.median(data), 3.5)

    def test_median_odd(self):
        data = [1, 2, 3, 4, 5, 6, 7]
        self.assertEqual(log_analyzer.median(data), 4)

    def test_median_single(self):
        data = [5]
        self.assertEqual(log_analyzer.median(data), 5)

    def test_median_empty(self):
        data = []
        self.assertIsNone(log_analyzer.median(data))


# python2 -m unittest  discover -s tests -v
if __name__ == '__main__':
    unittest.main()
